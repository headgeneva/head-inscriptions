# Projet Formulaires

Les cours:

16 cours pour les filières design

Chaque étudiant-e peut s’inscrire à 0, 1, 2 ou 3 cours

Types de cours:

- Cours Obligatoires - strictement par filière et semestre
- Cours Options Pro - uniquement CV
- Cours Options Libres

<table>
    <tr>
        <td style="background-color:lightgrey; font-weight:bold;">Filière</td>
        <td style="background-color:lightgrey; font-weight:bold;">Cours obligatoires</td>
        <td style="background-color:lightgrey; font-weight:bold;">Cours Options Pro.</td>
        <td style="background-color:lightgrey; font-weight:bold;">Cours Options Libres</td>
        <td style="background-color:lightgrey; font-weight:bold;">Workshops</td>
    </tr>
    <tr>
        <td style="font-weight:bold;">Architecture d'intérieur</td>
        <td>Lundi - Jeudi | cours fixes en fonction du semestre en cours, il faut s'inscrire à tout</td>
        <td>n/a</td>
        <td>N possibilités | peuvent s'inscrire dans d'autres filières (préciser lesquelles)</td>
        <td>N possibilités | peuvent s'inscrire dans d'autres filières (préciser lesquelles)</td>
    </tr>
    <tr>
        <td style="font-weight:bold;">Communication Visuelle | orientation Illu. ou E.M.</td>
        <td>Lundi - Mercredi | cours fixes en fonction du semestre en cours, il faut s'inscrire à tout</td>
        <td>Jeudi | peuvent s'inscrire dans d'autres filières, 0 à 3 choix obligatoires</td>
        <td>N possibilités | peuvent s'inscrire dans d'autres filières (préciser lesquelles)</td>
        <td>N possibilités | peuvent s'inscrire dans d'autres filières (préciser lesquelles)</td>
    </tr>
    <tr>
        <td style="font-weight:bold;">Design Mode</td>
        <td>Lundi - Jeudi | cours fixes en fonction du semestre en cours, il faut s'inscrire à tout</td>
        <td>n/a</td>
        <td>N possibilités | peuvent s'inscrire dans d'autres filières (préciser lesquelles)</td>
        <td>N possibilités | peuvent s'inscrire dans d'autres filières (préciser lesquelles)</td>
    </tr>
    <tr>
        <td style="font-weight:bold;">Design Bijou et Accessoire</td>
        <td>Lundi - Jeudi | cours fixes en fonction du semestre en cours, il faut s'inscrire à tout</td>
        <td>n/a</td>
        <td>N possibilités | peuvent s'inscrire dans d'autres filières (préciser lesquelles)</td>
        <td>N possibilités | peuvent s'inscrire dans d'autres filières (préciser lesquelles)</td>
    </tr>
</table>

# Besoins minimum: 

- Avoir des listes d'étudiants avec les options qu'ils ont choisis.
- Devenir maîtres du monde.
- Nombre maximal d'inscriptions par cours.
- Ouvrir ou fermer les inscriptions à dates précises. On peut aussi le fermer manuellement, en remplaçant le code par un message "Les inscriptions sont terminées".

# Grande question: un ou plusieurs formulaires

Est-ce qu'on crée 16 formulaires différents (un pour chaque cours), ou un formulaire avec les 16 options?

Eléments pour la décision:

On peut limiter avec Formidable le **nombre total de soumissions** pour un formulaire:
https://formidableforms.com/knowledgebase/frm_display_form_action/#kb-limit-total-number-of-entries

**Question: comment définir le nombre limite?**
- Solution: champ ACF

On peut aussi limiter un élément de menu déroulant:
https://formidableforms.com/knowledgebase/frm_setup_new_fields_vars/#kb-remove-option-after-n-submissions

***

# Fonctions:

**Limiter le nombre d'inscriptions pour un cours**
- À faire: un champ optionnel ajouté au formulaire qui permet d'entrer un nombre.


**Modification à posteriori**
- Permet à l'étudiant de voir où il est inscrit, et les modifier SI elles sont encore ouvertes.

- Réglages d'autorisation:
- On peut "Limiter la visibilité et la soumission du formulaire" : une fois la deadline passée, l'accès sera limité (p.ex. à Auteur)
- La "Modification des entrées" peut être activée ou désactivée.
- C'est un réglage à faire pour chaque formulaire.

***

# Identification des étudiants

On veut identifier:

- l'appartenance d'un étudiant à un semestre, 
- une filière
- statut de paiement de la taxe d'études = fonctionalité de base du site

Méthode: 

a) On crée des taxonomies: "Filière" (AI, E+C etc), éventuellement "Semestre" (de S1 à S6 ... peut aller jusqu'à 8)

b) Plugin custom qui contient une grande liste de données-étudiants et fait la vérification.
Format de données: fichier CSV?
Possible en PHP:
http://stackoverflow.com/questions/1269562/how-to-create-an-array-from-a-csv-file-using-php-and-the-fgetcsv-function

c) Template de page custom qui va afficher les cours disponibles pour sa filière (en fonction des catégories dans lesquelles on a mise les pages-formulaire).

Caté

***

# Fonction future: Calendrier / résumé 

Pour l'étudiant, un résumé des cours auxquels il est inscrit.

***