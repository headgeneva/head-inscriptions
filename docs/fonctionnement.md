# Fonctionnement

Les différentes composantes du projet:

## Thème WordPress custom

Thème enfant de "baskerville". Comporte un template qui effectue des tests conditionnels pour afficher (ou non) les formulaires d'inscription, en fonction des dates et du statut de l'utilisateur.

Ce template se trouve dans taxonomy-filiere.php (car les formulaires sont classés dans la Taxonomie "Filière").

## Les plugins utilisés:

### Plugin Formidable Pro

Utilisé pour créer les Formulaires.
Documentation: https://formidableforms.com/knowledgebase/

### Plugin ACF

Utilisé pour définir des options: 
- fichiers CSV avec listes des étudiants
- définition des dates ouverture / fermeture (champs: 'date_ouverture_inscriptions' et 'date_cloture_inscriptions'

Documentation: 

### Plugin HEAD Inscriptions Setup

- Déclare la taxonomie custom "filiere" (pour classer les formulaires d'inscription).
- Déclare la page Options de ACF.
- 

### Plugin HEAD Gestion étudiants

- Charge un fichier CSV et fait l'analyse des champs. 
- Retourne le contenu en Array PHP.

***

## Gestion des inscriptions

- Les inscriptions se font par l'intérmédiaire de formulaires "Formidable"
- Chaque formulaire est inséré dans une Page, ce qui permet de les classer par filières

### Limiter le nombre par cours

Une fonctionalité souhaitée est de pouvoir déterminer un nombre d'inscrits par cours.

Solutions possibles:
a) Ajouter un champ masqué à chaque formulaire
b) Utiliser un champ ACF (affectant la page qui contient le formulaire)

La première méthode n'étant pas documentée, nous avons utilisé la solution b, qui fonctionne très bien.

Méthode: https://formidablepro.com/knowledgebase/frm_display_form_action/#kb-limit-total-number-of-entries



ENSUITE:

Vues des inscriptions

- créer une vue "mes inscriptions", qui liste les cours auxquels on s'est inscrit = complication: il s'agit de nombreux formulaires différents - cette option n'exite pas dans les Vues Formidable.
- méthode lourde: créer une vue pour chaque formulaire, et la lister dans la page "Mes formulaires". Un peu pénible, mais faisable.

IMPORTANT: quand les inscriptions sont fermées: ces modifications doivent être bloquées!
Maybe: use javascript?

Question dans Forum: https://community.formidableforms.com/help-desk/delete-entries-without-modification/

***

- créer une vue Administrateur avec les inscrits pour tous les différents cours.

