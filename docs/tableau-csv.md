# Tableau CSV

L'une des fonctionalités du système que nous élaborons est l'identification des étudiants qui se connectent au site avec leur login AAI.

## La méthode retenue:

- Un fichier CSV est uploadé via l'interface admin WP
- Les données de ce CSV sont lues par notre plugin WP custom
- Les identifiants de l'utilisateur connecté sont matchées avec les données du CSV, pour établir quels sont ses droits d'accès aux formulaires.

Les colonnes importantes du CSV:

Données figurant dans les tableaux Excel fournis par la scolarité (par Lise Marine Ben Hassel):

- NO_PERSONNE_ID = on ne s'en sert pas
- NOM_PRENOM = on ne s'en sert pas (on peut utiliser les champs nom et prénom provenant de l'AD)
- EMAIL_UTILISATEUR = utilisé pour l'identification
- GPS_ACTUEL = contient le statut: "Media Design, 2016-2017, S4"
- STATUT = Présent-e / Absent-e / En échange international (OUT) / En échange (IN)

On peut donc réduire le tableau CSV à ces trois colonnes:

- EMAIL
- GPS
- STATUT

Ce que l'extension va faire:

- Charger le CSV via ACF
- Mettre à jour un champ optionnel spécial pour l'utilisateur: statut_HEAD
- Ce champ contient un array, avec 
- DPT: design ou arts_visuels
- GPS_ACTUEL
- STATUT

À l'affichage de chaque page, faire un test de ce champ.
Après un certain délai (48h?), le recharger.

