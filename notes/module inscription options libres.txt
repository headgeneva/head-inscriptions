Formulaire d'inscription options libres


head.hesge.ch/inscriptions/

À prévoir: 
- inscriptions aux cours libres
- inscriptions aux workshops

---

période d'ouverture:

il faut déterminer une date ouverture / on / off

attention, c'est plein en 30 secondes chrono!

- ouverture prioritaire pour les 3ème année 
- ouverture différée pour les autres

---

conditions à contrôler:

- nombre de modules choisissables ?
- entre zéro et trois.

- nombre d'inscrits par modules: défini, différent pour chaque module.
- liste pleines: indiquer qu'elles sont pleines, on ne peut plus s'y inscrire.

- Filières: 
- certains cours ne sont ouverts qu'à certaines filières
- p.ex. Domaine Design > Filière CV > Orientation Espace-Media

- gestion des priorités?
- on ne gère pas, sélection naturelle.

- Donner accès facilement au descriptif de chaque module.

- En-tête de formulaire: rappel des principales règles, 
- "le choix est ferme"
- "vous pouvez choisir entre zéro et trois options, en fonction de votre situation"

- Après soumission: 
- Affichage d'une confirmation
- Envoi d'un email de confirmation (à imprimer / confirmer)

À la fin du processus, une extraction des listes est transmise.
au BurScol

Il faut que chaque prof puisse avoir la liste des inscrits.


---

Liste de modules
chaque module: restriction des options
chaque module: nombre maximum de participants

--- 

Date ouverture des inscriptions:


