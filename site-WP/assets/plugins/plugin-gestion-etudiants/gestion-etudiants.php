<?php
/*
Plugin Name: HEAD Gestion Etudiants
Plugin URI: http://10.136.28.88/service-info/inscription-online/
Description: Plugin de fonctionalité pour le site HEAD Inscriptions.
Version: 0.1
Author: Infolab
Author URI: https://head.hesge.ch/infolab/
*/

// Ce plugin va effectuer un test sur un fichier CSV, pour vérifier le statut de l'étudiant.

/*
 * Fonction pour parcourir les données CSV
 ******************************************
*/

function head_status_test( $user_id ) {
	
	$user_data = array();
	
	$user_info = get_userdata( $user_id );

	// vérifier l'adresse mail de l'utilisateur
	
	$user_email = $user_info->user_email;

	$csv_design = head_load_csv('liste_etudiants_design');
	
	// loop through array and search email
	foreach ($csv_design as $i => $row) {

		if ( $row[0] == $user_email ) {
			
			// echo '<p>matching email: '. $row[0] .'</p>';
			// echo '<p>GPS_ACTUEL: '. $row[1] .'</p>';
			// echo '<p>STATUT: '. $row[2] .'</p>';
			
			// we got a match!
			
			// build an array with the data
			
			$user_data[] = $row[0];
			$user_data[] = $row[1];
			$user_data[] = $row[2];
			
			// end looping through CSV:
			// break the foreach loop
			
		}
	} 
	
	return $user_data;
			
//			echo '<pre>';
//			var_dump($csv_design);
//			echo '</pre>';
	
	// echo 'Username: ' . $user_info->user_login . "\n";
	// echo 'User roles: ' . implode(', ', $user_info->roles) . "\n";
	// echo 'User email: ' . $user_info->user_email . "\n";

}

/*
 * Fonction pour charger le fichier CSV
 *****************************************
*/

function head_load_csv( $file_id ) {
	
	// note: $file_id = name of ACF field: 
	// example: 'liste_etudiants_design'
	
	$csv_data = array();
	
	// use transient, in order to cache the data.
	
	if ( false === ( $csv_data = get_transient( 'head_'.$file_id ) ) ) {
	
		if ( function_exists('get_field') ) {
				
			$csv_design = get_field( $file_id, 'option');
	
			$csv_path = $csv_design['url'];
			
			$remove_path = get_site_url();
			
			$csv_path = ltrim($csv_path,$remove_path);
					
			// echo $csv_path;
			
			$csvFile = file('/home/e-smith/files/ibays/inscriptions/html/'.$csv_path);
	
			foreach ($csvFile as $line) {
			
			   $csv_data[] = str_getcsv($line);
			
			}
			
		} // end if function_exists
		
		// save transient for 1h
		set_transient( 'head_'.$file_id , $csv_data, 1 * HOUR_IN_SECONDS  ); 
	
	} // end get_transient test
	
	return $csv_data;

}