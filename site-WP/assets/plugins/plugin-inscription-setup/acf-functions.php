<?php
 
 // ACF options page
 // https://www.advancedcustomfields.com/resources/options-page/
 
 if( function_exists('acf_add_options_page') ) {
 	
 	acf_add_options_page();
 	
 }
 

/*
 * Test ouverture inscriptions
 * Retourne un array avec les champs suivants:
 * [status] = True / False
 * [class] = inscriptions-ouvertes ou inscriptions-fermees
 * [message] = message à afficher
*/

function head_test_ouverture_inscriptions() {
	
	$inscription = array();
	
	if ( function_exists('get_field') ) {
		
		// Régler la TimeZone du serveur
		date_default_timezone_set( get_option('timezone_string') );
		
		$date_now_unix = strtotime( date("Y-m-d H:i:s") );
		
		$date_ouverture_inscriptions = get_field( 
			'date_ouverture_inscriptions', 'option');
		$date_ouverture_unix = strtotime( $date_ouverture_inscriptions );
		
		$date_cloture_inscriptions = get_field( 
			'date_cloture_inscriptions', 'option');
		$date_cloture_unix = strtotime( $date_cloture_inscriptions );
		
		// cas 1: les inscriptions sont encore fermées:
		
		if ( $date_now_unix < $date_ouverture_unix ) {
			
			$inscription['status'] = false;
			$inscription['class'] = 'inscriptions-fermees';
			$inscription['message'] = 'Les inscriptions sont actuellement fermées, elles ouvriront le '. date_i18n( 'j F Y \à H:i \h', $date_ouverture_unix );
		
		// cas 2: les inscriptions sont déjà passées:
		
		} else if ( $date_now_unix > $date_cloture_unix ) {
			
			$inscription['status'] = false;
			$inscription['class'] = 'inscriptions-fermees';
			$inscription['message'] = 'Les inscriptions sont fermées.' ;
		
		// cas 3: les inscriptions sont ouvertes:
		
		} else {
			
			$inscription['status'] = true;
			$inscription['class'] = 'inscriptions-ouvertes';
			$inscription['message'] = 'Les inscriptions sont ouvertes.</br>Elles fermeront le '.date_i18n( 'j F Y \à H:i \h', $date_cloture_unix ) ;
			
		}
		
	} // end if 'get_field'
	
	return $inscription;

}
