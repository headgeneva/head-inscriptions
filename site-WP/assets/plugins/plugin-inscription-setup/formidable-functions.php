<?php
 

/*
 * Produce Form Output
 * For a post, produce the related form
 * Cette fonction permet simplement d'afficher un formulaire, selon l'ID.
*/

function head_frm_output ( $post_id ) {

	$terms = get_the_terms(
		$post->ID,
	    'id_formulaire'
	);

	if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
	    
	    foreach ( $terms as $term ) {
	    		// $shortode = 
	        $head_frm_output = do_shortcode('[formidable id='.$term->slug.']');
	    }
	    
	}
	
	return $head_frm_output;
}


/*
 * Get User Options
 * Returns a list of all forms to which a user responded
*/

function head_inscriptions_utilisateur ( $user_id ) {

	global $wpdb;
	
	$head_inscriptions_utilisateur = array();
	
	// Get forms that user has responded to
					
	$user_forms = $wpdb->get_col( $wpdb->prepare(
		"
		SELECT form_id 
		FROM ". $wpdb->prefix ."frm_items 
		WHERE user_id = %d
		", 
		$user_id ) 
	);
	

	if ( !empty($user_forms) ) {
	
		// We have the IDs of the forms
		
		// Now : get the IDs (previously: names)
		
		
		foreach ($user_forms as $i => $row) {

			$form_data = $wpdb->get_row( $wpdb->prepare(
				"
				SELECT id 
				FROM ". $wpdb->prefix ."frm_forms 
				WHERE id = %d
				", 
				$row ) 
			);
			
			// Available fields are: id, form_key, name, descritpion, parent_form_id, status...
			
			$head_inscriptions_utilisateur[] = $form_data->id;

		}
	}
	
	return $head_inscriptions_utilisateur;
}



/*
 * Form Name Shortcode
 * For usage in confirmation email
 * Returns name of current form
*/

function head_frm_name_shortcode( $atts ) {

	global $wpdb;
	
	$a = shortcode_atts( array(
			'id' => '',
		), $atts );
		
	if (!empty($a['id'])) {

		$form_id = $wpdb->get_row( $wpdb->prepare(
			"
			SELECT form_id 
			FROM ". $wpdb->prefix ."frm_items 
			WHERE id = %d
			", 
			$a['id']) 
		);
			
		$form_name = $wpdb->get_row( $wpdb->prepare(
			"
			SELECT name 
			FROM ". $wpdb->prefix ."frm_forms 
			WHERE id = %d
			", 
			$form_id->form_id ) 
		);
		
		return $form_name->name;
	
	}

}
add_shortcode( 'frm-form-name', 'head_frm_name_shortcode' );

