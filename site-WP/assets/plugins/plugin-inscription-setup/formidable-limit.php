<?php
 
/* 

Formidable Forms Limit Entries

- Limit Entries for Forms
- source: https://formidablepro.com/knowledgebase/frm_display_form_action/#kb-limit-total-number-of-entries
 */
 
add_action('frm_display_form_action', 'head_limit_entry_count', 8, 3);

function head_limit_entry_count($params, $fields, $form){

	remove_filter('frm_continue_to_new', '__return_false', 50);
	
	// Vérifier si les inscriptions sont ouvertes ou fermées
	
	$inscription = head_test_ouverture_inscriptions();
	
	// Pour les utilisateurs ADMIN, on affiche TOUJOURS les cours:
	if ( current_user_can( 'publish_posts' ) ) {
		$inscription['status'] = true;
	}
	
	if ( $inscription['status'] == false ) {
	
	
		// echo '<p class="inscriptions-fermees"> '. $inscription['message'] .'</p>';
		
		// Les inscriptions sont fermées!
		
		add_filter('frm_continue_to_new', '__return_false', 50);
	
	
	} else {
		
		// Les inscriptions sont ouvertes
		
		// Inscrits actuels:
		$count = FrmEntry::getRecordCount($form->id);
		
		// Nombre Maximum:
		// Get ACF field from current page
		
		if ( function_exists('get_field') ) {
			
			$head_max_number = get_field( "limite_inscriptions" );
			
			if ( empty( $head_max_number ) ) {
				
				// Get nombre_inscrits_par_defaut
				$head_max_number = get_field( 'nombre_inscrits_par_defaut', 'option');
				
			}
			
			// Vérifier s'il reste des places...
			if ( $head_max_number > $count ) {
				
				// Il reste des places
				
				$head_places_restantes = $head_max_number - $count;
				
				echo '<p class="nombre-inscrits">Maximum '. $head_max_number .' étudiant-e-s<br>';
				echo 'Places restantes: '. $head_places_restantes .'</p>';
				
			} else {
			
				// Le cours est complet!
			
				echo '<p class="nombre-inscrits cours-complet">Maximum '. $head_max_number .' étudiant-e-s<br>';
				echo 'Ce cours est complet</p>';
				
				add_filter('frm_continue_to_new', '__return_false', 50);
			
			} // vérification places 
			
		} // if get_field
		
	} // end testing inscription['status']
	
} // end function limit_entry_count()


/*
 * Validate entries upon submission
 * Check during submission if the course is full.
*/

add_filter('frm_validate_entry', 'head_validate_form', 10, 2);

function head_validate_form($errors, $values){
	
	if ( !function_exists('get_field') ) {
		return;
	}

	// We need to know the number of entries of current form.
	$count = FrmEntry::getRecordCount($values['form_id']);
	
	// Get max number of places for cours.
	$related = new WP_Query( array(
		'post_type' => 'post',
		'tax_query' => array(
			array(
				'taxonomy' => 'id_formulaire',
				'field'    => 'slug',
				'terms'    => $values['form_id'],
			),
		),
		'posts_per_page' => 1,
		'fields' => 'ids',
	) );
	
	$id_array = $related->posts;
	
	if ($id_array) {
		
		$id = $id_array[0];
		
		$head_max_number = get_field( "limite_inscriptions", $id );
		
	}
	
	if ( empty( $head_max_number ) ) {
		
		$head_max_number = get_field( 'nombre_inscrits_par_defaut', 'option');
		
	}
	
	if ( $count >= $head_max_number ) {
	
		$errors['my_error'] = 'Ce cours est complet.';
		
	}
	
	return $errors;
}