<?php
/*
Plugin Name: HEAD Inscriptions Setup
Plugin URI: http://10.136.28.88/service-info/inscription-online/
Description: Plugin de fonctionalité pour le site HEAD Inscriptions.
Version: 1.1
Author: Infolab
Author URI: https://head.hesge.ch/infolab/
*/


 
// Admin Interface Improvements

// Remove useless metaboxes :
 
 function head_remove_metaboxes() {
 
 	remove_meta_box( 'formatdiv' , 'post' , 'side' );
 	remove_meta_box( 'categorydiv' , 'post' , 'side' );
 	remove_meta_box( 'tagsdiv-post_tag' , 'post' , 'side' );
 	
 }
 add_action( 'admin_menu', 'head_remove_metaboxes' );
 
 
// Hide admin bar for students (subscribers) : 

 function head_hide_admin_bar() {
   if (!current_user_can('edit_posts')) {
     show_admin_bar(false);
   }
 }
 
 add_action('set_current_user', 'head_hide_admin_bar');


// Taxonomies

include_once (plugin_dir_path(__FILE__).'taxonomies.php');


// ACF Functions

include_once (plugin_dir_path(__FILE__).'acf-functions.php');


// Formidable Functions

include_once (plugin_dir_path(__FILE__).'formidable-functions.php');

// Formidable - limiter les inscriptions

include_once (plugin_dir_path(__FILE__).'formidable-limit.php');
