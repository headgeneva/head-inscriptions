<?php

// Déclaration des taxonomies:

function head_custom_taxonomies() 
{  	

	register_taxonomy( 
			'id_formulaire',
			array( 'post' ),
			array( 
		 		'hierarchical' => false, 
		 		'label' => 'ID Formulaire',
		 		'rewrite' => array('slug' => 'id-formulaire'),
		 		'singular_label' => 'ID Formulaire',
		 		'show_admin_column' => true
	 		) 
	);
	
	register_taxonomy( 
			'filiere',
			array( 'post' ),
			array( 
		 		'hierarchical' => true, 
		 		'label' => 'Filières',
		 		'rewrite' => array('slug' => 'filiere'),
		 		'singular_label' => 'Filière'
	 		) 
	);
	
	register_taxonomy( 
			'semestre',
			array( 'post' ),
			array( 
		 		'hierarchical' => true, 
		 		'label' => 'Semestre',
		 		'rewrite' => array('slug' => 'semestre'),
		 		'singular_label' => 'Semestre'
	 		) 
	);
 	 
  
} // end custom_taxonomies()  function

add_action( 'init', 'head_custom_taxonomies', 0 ); 
 
