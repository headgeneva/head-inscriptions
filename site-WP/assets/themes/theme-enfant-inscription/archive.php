<?php get_header(); ?>

<div class="wrapper section medium-padding">

	<div class="page-title section-inner">
		
		<h3><?php echo single_tag_title( '', false ); ?></h3>
		
		<?php
			$tag_description = tag_description();
			if ( ! empty( $tag_description ) )
				echo apply_filters( 'tag_archive_meta', '<div class="tag-archive-meta">' . $tag_description . '</div>' );
		?>
		
	</div> <!-- /page-title -->
	
	<div class="content section-inner">
	
		<?php 
		
			// Tester si les inscriptions sont ouvertes ou fermées.
			
			$inscription['status'] = false;
			$inscription['message'] = '';
			$custom_body_class = '';
			
			if ( function_exists('head_test_ouverture_inscriptions') ) {
				
				$inscription = head_test_ouverture_inscriptions();
				
				$custom_body_class .= $inscription['class'];
			
			}
			
			if ( !empty( $inscription['message'] ) ) {
			
				?>
				
				<div class="post header-inscriptions">
					<div class="post-content">
						<blockquote>
					
					<?php 
					
					echo $inscription['message']; 
					
					// pour administrateurs: ajouter heure actuelle du serveur
					
					if ( current_user_can( 'manage_options' ) ) {
					
						// date_default_timezone_set('UTC');
						// Europe/Zurich
						// http://php.net/manual/en/timezones.europe.php
						
						date_default_timezone_set( get_option('timezone_string') );
					
						echo '<br>Server time: '. date('d M Y H:i:s') ;
					}
					
					?>
					
						</blockquote>
					</div>
				</div>
				
				<?php
				
			}
						
			// Pour les utilisateurs ADMIN, on affiche TOUJOURS les cours:
			if ( current_user_can( 'publish_posts' ) ) {
				$inscription['status'] = true;
				$custom_body_class .= ' user-is-staff';
			}
			
			// if ( $inscription['status'] == true ) {
		
					if ( is_user_logged_in() ) {
					
						// get user status
						$user_id = get_current_user_id();
						
						
						
						// tester si déjà inscrit à 3 cours...
						
						if ( function_exists( 'head_inscriptions_utilisateur' ) ) {
						
							$mes_inscriptions = head_inscriptions_utilisateur ( $user_id );
							
							if ( count($mes_inscriptions) >= 3 ) {
								// has reached limit
								$custom_body_class .= ' reached-limit';
								
								// TODO: definir nombre en variable JS, mettre à jour à chaque clic.
							} 
							
						}
						
						// get current user email
//						if( function_exists('head_status_test') ) {
//							
//							$statut_HEAD = head_status_test( $user_id );
//							
//								//					echo '<pre>';
//								//					var_dump($statut_HEAD);
//								//					echo '</pre>';
//								
//								// TODO : basé sur le statut, tester si l'étudiant peut accéder aux formulaires...
//								
//								// 1) obtenir la filiere de l'étudiant
//								$current_user_filiere = '';
//								// explode
//								
//								// 2) obtenir le "slug" de la taxonomie actuelle
//								$taxonomy_slug = '';
								
							?>
							
							<?php if ( have_posts() ) : ?>
							
									<div class="posts <?php echo $custom_body_class; 
									?>">
									
										<?php rewind_posts(); ?>
									
										<?php while ( have_posts() ) : the_post(); ?>
										
											<div class="post-container">
										
												<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
										
													<?php get_template_part( 'content', 'formulaire' ); ?>
													
													<div class="clear"></div>
													
												</div> <!-- /post -->
											
											</div>
											
										<?php endwhile; ?>
													
									</div> <!-- /posts -->
												
									<?php if ( $wp_query->max_num_pages > 1 ) : ?>
									
										<div class="archive-nav">
										
											<?php echo get_next_posts_link( '&laquo; ' . __('Older posts', 'baskerville')); ?>
												
											<?php echo get_previous_posts_link( __('Newer posts', 'baskerville') . ' &raquo;'); ?>
											
											<div class="clear"></div>
											
										</div> <!-- /post-nav archive-nav -->
										
										<div class="clear"></div>
										
									<?php endif; ?>
											
								<?php endif; ?>
							
							<?php 
							
//						} else { // function_exists('head_status_test')
//						
//							echo '<p>head_status_test not available</p>';
//						}
		
			
					} else { // is_user_logged_in() 
					
					// User Not Logged In!!!
					
						?>
						
						<div class="post header-inscriptions">
		
							<!-- <div class="post-header">
								<h1 class="post-title">Inscriptions</h1>
							</div> -->
							
							<div class="post-content">
							
								<p>Veuillez <a href="<?php echo wp_login_url( get_permalink().'?version=10923482' ); ?>" title="Login">vous connecter avec votre login AAI</a> pour accéder à cette page.</p>
								<div class="clear"></div>
							</div> <!-- /post-content -->
							
						</div> <!-- /post -->
						
						<?php
					
					} // is_user_logged_in() 
					
			// } // if $inscription_open == true
	
			?>
	
	</div> <!-- /content -->

</div> <!-- /wrapper -->

<?php get_footer(); ?>