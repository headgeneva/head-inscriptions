<div class="post-header boite-formulaire">
	
    <h2 class="post-title"><?php the_title(); ?><?php 
    
    if ( current_user_can( 'publish_posts' ) ) {
    	echo '&nbsp;';
    	edit_post_link('🖋️');
    }
    
     ?></h2>
    
    <?php if( is_sticky() ) { ?> <span class="sticky-post"><?php _e('Sticky post', 'baskerville'); ?></span> <?php } ?>
    
</div> <!-- /post-header -->

<?php if ( has_post_thumbnail() ) : ?>

	<div class="featured-media">
	
		<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>">
		
			<?php the_post_thumbnail('post-thumbnail'); ?>
			
		</a>
				
	</div> <!-- /featured-media -->
		
<?php endif; ?>
									                                    	    
<div class="post-excerpt post-content">
	    		            			            	                                                                                            
	<?php 
	
	the_content(); 
	
	if ( function_exists('get_field') ) {
		
		$ou_et_quand = get_field( "ou_et_quand" );
		
		if ( $ou_et_quand ) {
			echo '<div class="ou-et-quand">'.$ou_et_quand.'</div>';
		}
	
	}
	
	// Produire le formulaire
	
	if ( function_exists('head_frm_output') ) {
		
		echo head_frm_output( $post->ID );
		
	}
	
	?>

</div> <!-- /post-excerpt -->
            
<div class="clear"></div>