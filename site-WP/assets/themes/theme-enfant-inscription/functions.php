<?php

/**
 * Infolab Theme functions and definitions
 *
 */

/*
 * Localisation
*/

add_action( 'after_setup_theme', 'my_child_theme_setup' );
function my_child_theme_setup() {
    load_theme_textdomain( 'baskerville', get_stylesheet_directory() . '/lang' );
}

/*
 * Theme Setup
*/

function theme_slug_setup() {
   add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'theme_slug_setup' );

/*
 * Load child theme style
*/

function infolab_load_style() {

	wp_enqueue_style( 'baskerville-child', get_stylesheet_directory_uri() . '/style.css' );
		
}

add_action('wp_print_styles', 'infolab_load_style', 11);

/*
 * Add Header Styles
*/

// menu-admin
function custom_header_css() {

	// is user editor or admin?
	
	if( current_user_can('publish_posts') ) {   
	    // stuff here for user roles that can publish_posts: editors and administrators, authors
	?>    
	  <style>
	     .menu-admin {
	     	display: block;
	     }
	     
	     .main-menu > :nth-last-child(2):before {
	     	display: block;
	     }
	  </style>  
	    
	<?php }
	
}
add_action('wp_head', 'custom_header_css');





// Remove footer wigets
// https://codex.wordpress.org/Function_Reference/unregister_sidebar



/* Allowed FileTypes
 ********************
 * method based on 
 * http://howto.blbosti.com/?p=329
 * List of defaults: https://core.trac.wordpress.org/browser/tags/3.8.1/src/wp-includes/functions.php#L1948
*/
add_filter('upload_mimes', 'custom_upload_mimes');
function custom_upload_mimes ( $existing_mimes=array() ) {
		// add your extension to the array
		$existing_mimes['svg'] = 'image/svg+xml';
		$existing_mimes['pkg'] = 'application/x-newton-compatible-pkg';
		$existing_mimes['epub'] = 'application/epub+zip';
		// removing existing file types
		unset( $existing_mimes['bmp'] );
		unset( $existing_mimes['tif|tiff'] );
		// and return the new full result
		return $existing_mimes;
}


/*
 * File Upload Security
 
 * Sources: 
 * http://www.geekpress.fr/wordpress/astuce/suppression-accents-media-1903/
 * https://gist.github.com/herewithme/7704370
 
 * See also Ticket #22363
 * https://core.trac.wordpress.org/ticket/22363
 * and #24661 - remove_accents is not removing combining accents
 * https://core.trac.wordpress.org/ticket/24661
*/ 
add_filter( 'sanitize_file_name', 'remove_accents', 10, 1 );
add_filter( 'sanitize_file_name_chars', 'sanitize_file_name_chars', 10, 1 );
 
function sanitize_file_name_chars( $special_chars = array() ) {
	$special_chars = array_merge( array( '’', '‘', '“', '”', '«', '»', '‹', '›', '—', 'æ', 'œ', '€','é','à','ç','ä','ö','ü','ï','û','ô','è' ), $special_chars );
	return $special_chars;
}


/**
 * remove WordPress Howdy
 * http://www.redbridgenet.com/?p=653
 ******************************/
function goodbye_howdy( $wp_admin_bar ) {
	$avatar = get_avatar( get_current_user_id(), 16 );
	if ( ! $wp_admin_bar->get_node( 'my-account' ) ) {
		return;
	}
	$wp_admin_bar->add_node( array(
			'id'    => 'my-account',
			'title' => sprintf( '%s', wp_get_current_user()->display_name ) . $avatar,
	) );
}
add_action( 'admin_bar_menu', 'goodbye_howdy' );


/* Dashboard improvement
******************************/
// http://wp.tutsplus.com/tutorials/customizing-wordpress-for-your-clients/
// http://www.wpbeginner.com/wp-tutorials/how-to-remove-wordpress-dashboard-widgets/
function lgm_remove_dashboard_widgets() {
	// Globalize the metaboxes array, this holds all the widgets for wp-admin
	global $wp_meta_boxes;
	
//	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
//	
	unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press'] );
//
//	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity'] );
	// RSS feeds:
	unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_primary'] );
}
add_action( 'wp_dashboard_setup', 'lgm_remove_dashboard_widgets' );


/* login interface
******************************/

//custom Login
function custom_login() { 
	echo '<link rel="stylesheet" type="text/css" href="'.get_stylesheet_directory_uri().'/login/login.css" />'; 
}
add_action('login_head', 'custom_login');

// src: http://codex.wordpress.org/Customizing_the_Login_Form

// change the link values so the logo links to your WordPress site
function my_login_logo_url() {
    return get_bloginfo( 'url' );
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
    return 'Intranet HEAD';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );

/* Taxonomy Filiere
 * Change order to be alphabetic
******************************/

function head_filiere_modify_query_order( $query ) {

    if ( $query->is_tax( 'filiere' ) && $query->is_main_query() ) {
        $query->set( 'orderby', 'title' );
        $query->set( 'order', 'ASC' );
        $query->set( 'posts_per_page', 50 );
    }
}
add_action( 'pre_get_posts', 'head_filiere_modify_query_order' );

