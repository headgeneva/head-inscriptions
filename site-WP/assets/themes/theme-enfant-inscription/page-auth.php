<?php 

/**
 *
 * @package InfoLab
 * Template Name: HEAD Authentification
 */

get_header(); ?>

<div class="wrapper section medium-padding">
										
	<div class="section-inner">
	
		<div class="content full-width">
	
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
				<div class="post">
				
				<?php 
				
				if ( is_user_logged_in() ) {
				
				
				// get user status
				$user_id = get_current_user_id();
				
				// echo '<p>current user ID:'.$user_id.'</p>';
				
				// get current user email
//				if( function_exists('head_status_test') ) {
//					
//					$statut_HEAD = head_status_test( $user_id );
//					
//				} else {
//				
//					echo '<p>head_status_test not available</p>';
//				}
				
				?>
				<div class="post-header">
				    <h1 class="post-title"><?php the_title(); ?></h1>
				    				    
			    </div> <!-- /post-header -->
			
				<?php if ( has_post_thumbnail() ) : ?>
					
					<div class="featured-media">
						<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>">
							<?php the_post_thumbnail('post-image'); ?>
							<?php if ( !empty(get_post(get_post_thumbnail_id())->post_excerpt) ) : ?>
											
								<div class="media-caption-container">
								
									<p class="media-caption"><?php echo get_post(get_post_thumbnail_id())->post_excerpt; ?></p>
								</div>
							<?php endif; ?>
							
						</a>
					</div> <!-- /featured-media -->
						
				<?php endif; ?>
			   				        			        		                
				<div class="post-content">
					<?php the_content(); ?>
					<div class="clear"></div>
				</div> <!-- /post-content -->
						
						
						<?php // end of the loop. 
						
					} else {
					
						?>
						
						<div class="post-header">
							    <h1 class="post-title"><?php the_title(); ?></h1>
							    				    
						    </div> <!-- /post-header -->
						
							
							<div class="post-content">
								<p>Veuillez <a href="<?php echo wp_login_url( get_permalink().'?version=10923482' ); ?>" title="Login">vous connecter avec votre login AAI</a> pour accéder à cette page.</p>
								<div class="clear"></div>
							</div> <!-- /post-content -->
							
						
						<?php
					
					}
				
				 ?>

									
				</div> <!-- /post -->
			
			<?php endwhile; else: ?>
			
				<p><?php _e("We couldn't find any posts that matched your query. Please try again.", "baskerville"); ?></p>
		
			<?php endif; ?>
		
			<div class="clear"></div>
			
		</div> <!-- /content -->
				
		<div class="clear"></div>
	
	</div> <!-- /section-inner -->

</div> <!-- /wrapper -->
								
<?php get_footer(); ?>