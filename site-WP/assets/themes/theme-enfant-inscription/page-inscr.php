<?php 

/**
 *
 * @package InfoLab
 * Template Name: HEAD Mes Inscriptions
 */

get_header(); ?>

<div class="wrapper section medium-padding">
										
	<div class="section-inner">
	
		<div class="content full-width">
	
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
				<div class="post">
				
				<?php 
				
				if ( is_user_logged_in() ) {
				
				// get user status
				$user_id = get_current_user_id();
				
				$inscription['status'] = false;
				$inscription['message'] = '';
				$custom_body_class = '';
				
				if ( function_exists('head_test_ouverture_inscriptions') ) {
					
					$inscription = head_test_ouverture_inscriptions();
					$custom_body_class .= $inscription['class'];
				
				}
				
				// Pour les utilisateurs ADMIN, on affiche TOUJOURS les cours:
				if ( current_user_can( 'publish_posts' ) ) {
					$inscription['status'] = true;
					$custom_body_class .= ' user-is-staff';
				}
				
			
				?>
				<div class="post-header">
				    <h1 class="post-title"><?php the_title(); ?></h1>
				    				    
			    </div> <!-- /post-header -->
			
				<?php if ( has_post_thumbnail() ) : ?>
					
					<div class="featured-media">
						<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>">
							<?php the_post_thumbnail('post-image'); ?>
							<?php if ( !empty(get_post(get_post_thumbnail_id())->post_excerpt) ) : ?>
											
								<div class="media-caption-container">
								
									<p class="media-caption"><?php echo get_post(get_post_thumbnail_id())->post_excerpt; ?></p>
								</div>
							<?php endif; ?>
							
						</a>
					</div> <!-- /featured-media -->
						
				<?php endif; ?>
			   				        			        		                
				<div class="post-content <?php echo $custom_body_class; ?>">
				
					<?php 
					
					// Afficher les inscriptions de la personne:
					
					if ( function_exists( 'head_inscriptions_utilisateur' ) ) {
					
						$mes_inscriptions = head_inscriptions_utilisateur ( $user_id );
												
						if (!empty($mes_inscriptions)) {
						
							echo '<p class="large-text">Vous êtes inscrit•e aux Options Libres suivantes:</p>';
							echo '<div class="masonry-container posts">';
							
							foreach ($mes_inscriptions as $i => $form_id) {
							
								echo '<div class="detail-mes-inscriptions post-container">';
								echo '<div class="mes-inscriptions-inner">';
							
								// Get related post, via taxonomy:
								
								$details_inscription = new WP_Query( array(
									'post_type' => 'post',
									'tax_query' => array(
										array(
											'taxonomy' => 'id_formulaire',
											'field'    => 'slug',
											'terms'    => $form_id,
										),
									),
								) );
								
								if ( $details_inscription->have_posts() ) : ?>
								
						 				<?php while ( $details_inscription->have_posts() ) : $details_inscription->the_post(); ?>
						 				
						 				<?php 
						 				
						 					echo '<h3 class="pretty-title">'.get_the_title().'</h3>';
						 				
						 				?>
						 
						 					<div class="grid">
						 						<?php 
						 						
						 						the_content(); 
						 						
						 						if ( function_exists('get_field') ) {
						 							
						 							$ou_et_quand = get_field( "ou_et_quand" );
						 							
						 							if ( $ou_et_quand ) {
						 								echo '<div class="ou-et-quand">'.$ou_et_quand.'</div>';
						 							}
						 							
						 						}
						 						
						 						// Formulaire:

						 						echo do_shortcode('[formidable id='.$form_id.']');
						 						
						 						?>
						 					</div><!-- .grid -->
						 
						 				<?php endwhile;
						 				
									endif;
									wp_reset_postdata();
									
								echo '</div></div><!-- .detail -->';

							} // foreach $mes_inscriptions
							
							echo '</div>';
							
						} // !empty
					
					} // function exists

					?>
					<div class="clear"></div>
					
					<?php the_content(); ?>
					<div class="clear"></div>

				</div> <!-- /post-content -->
		
						<?php // end of the main loop. 
						
					} else {
					
						?>
						
						<div class="post-header">
							    <h1 class="post-title"><?php the_title(); ?></h1>
							    				    
						    </div> <!-- /post-header -->
						
							
							<div class="post-content">
								<p>Veuillez <a href="<?php echo wp_login_url( get_permalink().'?version=10923482' ); ?>" title="Login">vous connecter avec votre login AAI</a> pour accéder à cette page.</p>
								<div class="clear"></div>
							</div> <!-- /post-content -->
							
						
						<?php
					
					} // end testing if logged in.
				
				 ?>
			
				</div> <!-- /post -->
			
			<?php endwhile; else: ?>
			
				<p><?php _e("We couldn't find any posts that matched your query. Please try again.", "baskerville"); ?></p>
		
			<?php endif; ?>
		
			<div class="clear"></div>
			
		</div> <!-- /content -->
				
		<div class="clear"></div>
	
	</div> <!-- /section-inner -->

</div> <!-- /wrapper -->
								
<?php get_footer(); ?>